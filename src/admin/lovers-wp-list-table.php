<?php

class Customers_List extends Base_Customers_List{
	public function __construct() {
		$args = [
			'singular' => __( 'Note', 'sp' ),
			'plural'   => __( 'Notes', 'sp' ),
			'ajax'     => false,
		];
		$main_table_name = 'customers';
		
		$columns = [
			'name'    => __( 'Name', 'sp' ),
			'city'    => __( 'City', 'sp' ),
			// 'birth_date'    => __( 'BirthDate', 'sp' ),
			'updated_at' => __('Updated At', 'sp'),
		];

		$sortable_columns = [
			'name' => array( 'name', true ),
			// 'birth_date' => array( 'birth_date', false )
		];

		Parent::__construct($args, $main_table_name, $columns, $sortable_columns);
		
		$this->init();
		// $this->setup_tables();
		// $this->seed_lovers_data();
	}

	public function setup_tables() {
		global $wpdb;

		$questions_table_name = 'belfab_questions';
		$questions_table_sql = "
			question_text varchar(255) NOT NULL,
			is_first_question varchar(1) DEFAULT 0,
			test varchar(255) NULL,
		";
		
		if (!$this->table_exists($questions_table_name)) {
			$this->create_table($questions_table_name, $questions_table_sql);
		}

		$answers_table_name = 'belfab_answers';
		$answers_prefixed_table_name = $wpdb->base_prefix . $questions_table_name;
		$answers_table_sql = "
			answer_text varchar(255) NOT NULL,
			next_step_value varchar(255) NOT NULL,	
			question_id INT NOT NULL,

			FOREIGN KEY (question_id) REFERENCES $answers_prefixed_table_name (ID) ON DELETE CASCADE,
		";

		$this->create_table($answers_table_name, $answers_table_sql);
	}

	public function setup_main_table() {
		$table_name = self::$main_table_name;
		$sql = "
		name varchar(255) NOT NULL,
		birth_date varchar(255) NULL,
		city varchar(255) NULL,
		";
		$this->create_table($table_name, $sql);
	}

	public function column_name($item) {
		return $this->__custom__main_column($item, 'name');
	}
}

class SP_Plugin extends Base_SP_Plugin {
	public function __construct() {
		$this->main_page_title = 'Customers';
		$this->main_page_slug = 'customers-list';
		$this->main_page_menu = 'Customers';
		// $this->main_page = 'custom_wp_list_table_class';
		$this->new_item_page = 'new_item';
		
		Parent::__construct(
			$this->new_item_page,
			$this->main_page_title,
			$this->main_page_menu,
			$this->main_page_slug,
		);
		
		// add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
		// add_action('admin_menu', [$this, 'add_admin_pages']);

		$this->add_example_pages();
	}

	public function screen_option() {
		$option = 'per_page';
		$args   = [
			'label'   => 'Customers',
			'default' => 5,
			'option'  => 'customers_per_page',
		];

		add_screen_option( $option, $args );

		$this->item_obj = new Customers_List();
	}

	public function add_example_pages() {
		add_action( 'admin_menu', function() {
			add_menu_page(
				'example page title',
				'All Examples',
				'manage_options',
				'example-slug',
				function() {
					?>
						<div>THE EXAMPLE....</div>
					<?php
				},
				'dashicons-editor-removeformatting',
				1,
			);	
		});

		add_action('admin_menu', function() {
			add_submenu_page(
				'example-slug',
				'Add a Example',
				'Add Example ',
				'manage_options',
				'sub-example-slug',
				function() {
					?>
						<div>THE EXAMPLE PAGE !!!</div>
					<?php
				}
			);
		});

		add_action('admin_menu', function() {
			add_submenu_page(
				'example-slug',
				'Edit a Example',
				'Edit Example ',
				'manage_options',
				'edit-sub-example-slug',
				function() {
					?>
						<div>THE EDIT EXAMPLE PAGE !!!</div>
					<?php
				}
			);
		});
	}

	public function add_item_template() {
		?>
			<div>THE Portez ce w !!!</div>
		<?php
	}

	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}

class Lovers_List_Table extends Base_Customers_List{
	public function __construct() {
		$args = [
			'singular' => __( 'Note', 'sp' ),
			'plural'   => __( 'Notes', 'sp' ),
			'ajax'     => false,
		];

		$main_table_name = 'lovers';
		
		$columns = [
			'city'    => __( 'City', 'sp' ),
			'name'    => __( 'Name', 'sp' ),
			'birth_date'    => __( 'BirthDate', 'sp' ),
			'updated_at' => __('Updated At', 'sp'),
		];

		$sortable_columns = [
			'name' => array( 'name', true ),
			'city' => array( 'city', true ),
			'birth_date' => array( 'birth_date', false )
		];

		$edit_page_slug = 'new-lover';

		Parent::__construct($args, $main_table_name, $columns, $sortable_columns, $edit_page_slug);
		
		$this->init();
		// $this->setup_tables();
		// $this->seed_lovers_data();
	}

	public function setup_tables() {
		global $wpdb;

		$questions_table_name = 'belfab_questions';
		$questions_table_sql = "
			question_text varchar(255) NOT NULL,
			is_first_question varchar(1) DEFAULT 0,
			test varchar(255) NULL,
		";
		
		if (!$this->table_exists($questions_table_name)) {
			$this->create_table($questions_table_name, $questions_table_sql);
		}

		$answers_table_name = 'belfab_answers';
		$answers_prefixed_table_name = $wpdb->base_prefix . $questions_table_name;
		$answers_table_sql = "
			answer_text varchar(255) NOT NULL,
			next_step_value varchar(255) NOT NULL,	
			question_id INT NOT NULL,

			FOREIGN KEY (question_id) REFERENCES $answers_prefixed_table_name (ID) ON DELETE CASCADE,
		";

		$this->create_table($answers_table_name, $answers_table_sql);
	}

	public function setup_main_table() {
		$table_name = self::$main_table_name;
		$sql = "
		name varchar(255) NOT NULL,
		birth_date varchar(255) NULL,
		city varchar(255) NULL,
		";
		$this->create_table($table_name, $sql);
	}

	public function seed_lovers_data()
    {
      global $wpdb;
  
      for ($i = 1; $i < 11; ++$i) {
        $data = array(
          "name" => "Lover's name N° " . $i,
          "city" => "Lover's  city N° " .$i,
          "birth_date" => "Lover's birth_date N° " .$i,
        );
        $wpdb->insert(self::$prefixed_main_table_name, $data);
      }
    }

	public function column_city($item) {
		return $this->__custom__main_column($item, 'city');
	}
}

class Lovers_SP_Plugin extends Base_SP_Plugin {
	
	public function __construct() {
		$this->main_page_title = 'Lovers';
		$this->main_page_slug = 'lovers-list';
		$this->main_page_menu = 'Lovers';
		$this->main_page_menu_icon = 'dashicons-editor-unlink';
		$this->new_item_page_slug = 'new-lover';
		
		Parent::__construct(
			$this->new_item_page_slug,
			$this->main_page_title,
			$this->main_page_menu,
			$this->main_page_slug,
			$this->main_page_menu_icon,
		);
	}

	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Lovers',
			'default' => 5,
			'option'  => 'lovers_per_page'
		];

		add_screen_option( $option, $args );

		$this->item_obj = new Lovers_List_Table();
	}

	public function add_item_template() {
		?>
			<div>Adding a new Lover !!!</div>
		<?php
	}

	public static function get_instance() {
		// if ( true || ! isset( self::$instance ) ) {
			self::$instance = new self();
		// }
		return self::$instance;
	}
}

class Cars_List_Table extends Base_Customers_List{
	public function __construct() {
		$args = [
			'singular' => __( 'Car', 'sp' ),
			'plural'   => __( 'Cars', 'sp' ),
			'ajax'     => false,
		];

		$main_table_name = 'cars';
		
		$columns = [
			'model'    => __( 'Model', 'sp' ),
			'name'    => __( 'Name', 'sp' ),
			'size'    => __( 'Size', 'sp' ),
			'updated_at' => __('Updated At', 'sp'),
		];

		$sortable_columns = [
			'name' => array( 'name', true ),
			'model' => array( 'model', true ),
			'size' => array( 'size', false )
		];

		$edit_page_slug = 'new-car';

		Parent::__construct($args, $main_table_name, $columns, $sortable_columns, $edit_page_slug);
		
		$this->init();
		// $this->setup_tables();
		// $this->seed_main_table_data();
	}

	public function setup_main_table() {
		$table_name = self::$main_table_name;
		$sql = "
			name varchar(255) NOT NULL,
			size varchar(255) NOT NULL,
			model varchar(255) NOT NULL,
		";
		$this->create_table($table_name, $sql);
	}

	public function seed_main_table_data()
    {
      global $wpdb;
  
      for ($i = 1; $i < 11; ++$i) {
        $data = array(
          "name" => "Toyota N° " . $i,
          "model" => "Berline " .$i,
          "size" => 44 * $i . " x " . 10 * $i + 1,
        );
        $wpdb->insert(self::$prefixed_main_table_name, $data);
      }
    }

	public function column_model($item) {
		return $this->__custom__main_column($item, 'model');
	}
}

class Cars_SP_Plugin extends Base_SP_Plugin {
	
	public function __construct() {
		$this->main_page_title = 'Cars';
		$this->main_page_slug = 'Cars-list';
		$this->main_page_menu = 'Cars';
		$this->main_page_menu_icon = null; // 'dashicons-editor-unlink';
		$this->new_item_page_slug = 'new-car';
		
		Parent::__construct(
			$this->new_item_page_slug,
			$this->main_page_title,
			$this->main_page_menu,
			$this->main_page_slug,
			$this->main_page_menu_icon,
		);
	}

	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Cars',
			'default' => 5,
			'option'  => 'cars_per_page'
		];

		add_screen_option( $option, $args );

		$this->item_obj = new Cars_List_Table();
	}

	public function add_item_template() {
		?>
			<div>
				<h1>Create a new Car</h1>
			</div>
		<?php
	}

	public static function get_instance() {
		// if ( true || ! isset( self::$instance ) ) {
			self::$instance = new self();
		// }
		return self::$instance;
	}
}
