<?php

/*
Plugin Name: ZeniData Base Plugin Simple
Plugin URI: https://zenidata.com
Description: ZeniData Base Plugin Simple
Version: 1.0
Author: ZeniData
Author URI:  https://zenidata.com
*/

include 'lib/zenidata-base-plugin-simple-core-lib/core/base-wp-list-table.php';

class Student_WP_List_Table extends Base_WP_List_Table {
    public function __construct() {
		$args = [
			'singular' => __( 'Student', 'sp' ),
			'plural'   => __( 'Students', 'sp' ),
			'ajax'     => false,
		];

		$main_table_name = 'students';
		
		$columns = [
			'firstname'    => __( 'Firstname', 'sp' ),
			'lastname'    => __( 'Lastname', 'sp' ),
			'age'    => __( 'Age', 'sp' ),
			'updated_at' => __('Updated At', 'sp'),
		];

		$sortable_columns = [
			'firstname' => array( 'firstname', true ),
			'lastname' => array( 'lastname', true ),
			'age' => array( 'age', false )
		];

		$edit_page_slug = 'edit-student';
		
		Parent::__construct($args, $main_table_name, $columns, $sortable_columns, $edit_page_slug);
		
		$this->init();
		// $this->setup_tables();
		// $this->seed_main_table_data();
	}

    // public function setup_main_table() {
	// 	$table_name = self::$main_table_name;
	// 	$sql = "
	// 		firstname varchar(255) NOT NULL,
	// 		lastname varchar(255) NOT NULL,
	// 		age varchar(255) NOT NULL,
	// 	";
	// 	$this->create_table($table_name, $sql);
	// }

    // public function seed_main_table_data()
    // {
    //   global $wpdb;
  
    //   for ($i = 1; $i < 11; ++$i) {
    //     $data = array(
    //       "firstname" => "Firstname " . $i,
    //       "lastname" => "Lastname " .$i,
    //       "age" => 2 * $i,
    //     );
    //     $wpdb->insert(self::$prefixed_main_table_name, $data);
    //   }
    // }

    public function column_firstname($item) {
		return $this->__custom__main_column($item, 'firstname');
	}
}

class Students_SP_Plugin extends Base_SP_Plugin {

    public function __construct() {
        $this->main_page_title = 'Students';
		$this->main_page_slug = 'students-list';
		$this->main_page_menu = 'Students';
		$this->main_page_menu_icon = null; // 'dashicons-editor-unlink';
		$this->new_item_page_slug = 'new-student';
		
		$this->edit_item_page_slug = 'edit-student';
		
		Parent::__construct(
			$this->new_item_page_slug,
			$this->main_page_title,
			$this->main_page_menu,
			$this->main_page_slug,
			$this->main_page_menu_icon,
		);

		$this->add_pages();
    }

    public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Students',
			'default' => 5,
			'option'  => 'students_per_page'
		];

		add_screen_option( $option, $args );

		$this->item_obj = new Student_WP_List_Table();
	}

	public function add_item_template() {
		include_once 'pages/add-student.php';
	}

	public function add_pages() {
		add_action('admin_menu', function() {
			add_submenu_page(
				'',
				'Edit',
				'Edit ',
				'manage_options',
				$this->edit_item_page_slug,
				function() {
					include_once 'pages/edit-student.php';
				}
			);
		});
	}

    public static function get_instance() {
		// if ( true || ! isset( self::$instance ) ) {
			self::$instance = new self();
		// }
		return self::$instance;
	}
}

add_action('plugins_loaded', function () {
    Students_SP_Plugin::get_instance();
});
  
  